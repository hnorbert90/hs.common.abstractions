﻿namespace HS.Common.Abstractions.Pagination
{
    public interface IPagedQueryRequest
    {
        int PageIndex { get; set; }
        int PageSize { get; set; }
    }
}
