﻿namespace HS.Common.Abstractions.Pagination
{
    public interface IPagedQueryResponse
    {
        int TotalPages { get; set; }
    }
}
