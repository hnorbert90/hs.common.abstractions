﻿namespace HS.Common.Abstractions.System
{
    using global::System;

    public interface IDateTimeProvider
    {
        DateTime Now { get; }
        DateTime UtcNow { get; }
        DateTime Today { get; }
        DateTime MaxValue { get; }
        DateTime MinValue { get; }
    }
}